#ifndef __PROGTEST__
#include <stdio.h>
#include <assert.h>
#endif /* __PROGTEST__ */

int dateToIndex(int day, int month, int year, int *idx) {
    if (year < 2000 || month > 12 || month <= 0 || day <= 0 || day >= 32)
        return 0;
    int prestupny = 0;

    if (year % 4000 == 0)
        prestupny = 0;
    else if (year % 400 == 0)
        prestupny = 1;
    else if (year % 100 == 0)
        prestupny = 0;
    else if (year % 4 == 0)
        prestupny = 1;


    if ( (prestupny == 0 && month == 2 && day >= 29) ||
         (prestupny == 1 && month == 2 && day >= 30) )
        return 0;
    if ( (month == 3 || month == 1 || month == 5 || month == 7 || month == 8 || month == 10 || month == 12) && day >= 32)
        return 0;
    if ( ( month == 4 || month == 6 || month == 9 || month == 11 ) && day >= 31)
        return 0;


    int month_sum = 0;
    for (int i = 1 ; i < month ; i++){
        if ( i == 2 && prestupny)
            month_sum += 29;
        else if ( i == 2 )
            month_sum += 28;
        else if (i == 3 || i == 1 || i == 5 || i == 7 || i == 8 || i == 10 || i == 12)
            month_sum += 31;
        else
            month_sum += 30;
    }
    int sum = month_sum + day;
    *idx = sum;
    return 1;
}



#ifndef __PROGTEST__
int main (int argc, char * argv []) {
    int idx;
    assert(dateToIndex(31, 6, 430800, &idx) == 0);

    assert(dateToIndex(31, 2, 262800, &idx) == 0);
    assert(dateToIndex(1, 11, 121600, &idx) == 1 && idx == 306);

    assert(dateToIndex( 1,  1, 2000, &idx) == 1 && idx == 1);
    assert(dateToIndex( 1,  2, 2000, &idx) == 1 && idx == 32);
    assert(dateToIndex(29,  2, 2000, &idx) == 1 && idx == 60);
    assert(dateToIndex(29,  2, 2001, &idx) == 0);

    assert(dateToIndex( 1, 12, 2000, &idx) == 1 && idx == 336);
    assert(dateToIndex(31, 12, 2000, &idx) == 1 && idx == 366);
    assert(dateToIndex( 1,  1, 1999, &idx) == 0);
    assert(dateToIndex( 6,  7, 3600, &idx) == 1 && idx == 188);
    assert(dateToIndex(29,  2, 3600, &idx) == 1 && idx == 60);
    assert(dateToIndex(29,  2, 4000, &idx) == 0);
    return 0;
}
#endif /* __PROGTEST__ */

