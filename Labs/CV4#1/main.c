#include <stdio.h>
#include <math.h>
#include <float.h>

int main() {
    printf("Zadejte rovnici:\n");
    double x , y , userResult;
    char op[2] = {'0' , '\0'};
    if ( scanf("%lf %s %lf = %lf" , &x ,&op[0] , &y , &userResult) != 4 ){
        printf("Nespravny vstup.\n");
        return 1;
    }
    if ( op[0] != '+' && op[0] != '-' && op[0] != '/' && op[0] != '*'  ){
        printf("Nespravny vstup.\n");
        return 2;
    }
    double myResult = 0;
    if (op[0] == '+')
        myResult = x + y;
    else if (op[0] == '-')
        myResult = x - y;
    else if (op[0] == '*')
        myResult = x * y;
    else if (op[0] == '/') {
        if (y == 0){
            printf("Nespravny vstup.\n");
            return 3;
        }
        double tmp = fmod(x,y);
        x -= tmp;
        myResult = x / y;
    }
    if ( fabs(myResult-userResult) <= (100 * DBL_EPSILON *userResult) || myResult == userResult)
        printf("Rovnice je spravne.\n");
    else {
        printf("%g != %g\n", myResult, userResult);
    }

    return 0;
}
