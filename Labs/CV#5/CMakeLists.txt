cmake_minimum_required(VERSION 3.17)
project(CV_5 C)

add_executable(CV_5 main.c )
target_link_libraries(CV_5 -lm)