#include <stdio.h>
#include <math.h>
#include <stdlib.h>


int main() {
    printf("Rozsah:\n");
    int size;
    if (scanf("%d" , &size) != 1) {
        printf("Nespravny vstup.\n");
        return 1;
    }
    if (size <= 0){
        printf("Nespravny vstup.\n");
        return 2;
    }
    int width = floor(log10(fabs(size*size))) + 1 + 1;

    char * emptyBox = (char*)malloc(width + 1);
    for (int i = 0 ; i <= width + 1 ; i++){
        if (i == width)
            emptyBox[i] = '|';
        else if (i == width - 1)
            emptyBox[i] = '\0';
        else
            emptyBox[i] = ' ';
    }
    // printing 1st line
    for (int i = size+1; i >= 1 ; i-- ){
        if (i == size+1)
            printf("%s|",emptyBox);
        else{
            printf("%*d",width,i);
        }
    }
    printf("\n");

    for(int i = 0 ; i <= size; i++){
        if (i == 0) {
            for (int j = 0; j < width - 1; j++)
                printf("-");
            printf("+");
        }
        else
            for (int j = 0; j <= width-1; j++)
                printf("-");
    }
    printf("\n");

    // NOW 2 for loops to generate and print numbers
    int tmp = 1;
    for (int i = 1 ; i <= size  ; i++ ){ // i: 1 - 10
        printf("%*d|",width-1,i); // i: 1 - 10
        for (int j = size; j >= tmp ; j--){ // j: 10 - 1 -> 9 - 1
            printf("%*d", width,j*i);
        }
        tmp++;
        printf("\n");
    }
    free(emptyBox);
    return 0;
}
