#include<stdio.h>
#include<stdlib.h>
#include<string.h>


typedef struct Person{
    const char * name;
    int age;
}Person;

int GetInput(int * r, int * g, int * b){ //return 1 on fail
    char * x = (char*)malloc(100);
    if ( scanf(" rgb ( %d , %d , %d %s ", r , g ,b , x) == 4 ) {
        if ( (*r > 255 || *r < 0) || (*g > 255 || *g < 0) || (*b > 255 || *b < 0) ) {
            free(x);
            return 1;
        }
        else {
            free(x);
            return 0;
        }
    }
    else {
        free(x);
        return 2;
    }
}

void GetHex(int num , char * Hex){
    if (num != 0) {
        int koe = num;
        int index = 1;
        int tmp;
        char x = '0';
        while (koe != 0) {
            tmp = koe % 16;
            if (tmp >= 10) {
                if (tmp == 10)
                    x = 'A';
                if (tmp == 11)
                    x = 'B';
                if (tmp == 12)
                    x = 'C';
                if (tmp == 13)
                    x = 'D';
                if (tmp == 14)
                    x = 'E';
                if (tmp == 15)
                    x = 'F';
            } else {
                //sprintf(x, "%d", tmp);
                tmp += 48;
                x = tmp;
            }
            Hex[index] = x;
            index--;
            koe = koe / 16;
        }
    }

}

int main(void){
    printf("Zadejte barvu v RGB formatu:\n");
    int r = 0,g = 0,b = 0;
    int _exitStatus = GetInput(&r,&g,&b);
    if (_exitStatus) {
        printf("Nespravny vstup.\n");
        return _exitStatus;
    }
    char Hex1[] = {'0' , '0' , '\0'};
    char Hex2[] = {'0' , '0' , '\0'};
    char Hex3[] = {'0' , '0' , '\0'};
    GetHex(r , Hex1);
    GetHex(g , Hex2);
    GetHex(b , Hex3);

    printf("#%s%s%s\n" , Hex1 , Hex2, Hex3 );
    return 0;
}