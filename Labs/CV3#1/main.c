#include <stdio.h>

typedef struct result {
    int _hours, _minutes, _secs, _msec;
} result;


int GetInput(int * hours, int * minutes, int * secs, int * msec){ //return 1 on fail
    if ( scanf("%d : %d : %d , %d", hours, minutes, secs, msec) == 4 ) {
        if ( (*hours < 0 || *minutes < 0 || *secs < 0 || *msec < 0 ) ||
             (*hours >= 24 || *minutes >= 60 || *secs >= 60 || *msec >= 1000 )     )
            return 1;
        else
            return 0;
    }
    else
        return 1;
}


int main(void){
    int hours = 0, minutes = 0, secs = 0, msec = 0;
    int hours2 = 0, minutes2 = 0, secs2 = 0, msec2 = 0;
    printf("Zadejte cas t1:\n");
    if ( GetInput(&hours, &minutes, &secs, &msec) ) {
        printf("Nespravny vstup.\n");
        return 1;
    }
    printf("Zadejte cas t2:\n");
    if ( GetInput(&hours2, &minutes2, &secs2, &msec2) ) {
        printf("Nespravny vstup.\n");
        return 1;
    }



    long long t1 = msec + secs*1000 + minutes*60*1000 + hours*60*60*1000;
    long long t2 = msec2 + secs2*1000 + minutes2*60*1000 + hours2*60*60*1000;
    //printf("t2 < t1 -> %lld < %lld" , t2,t1);
    if (t2 < t1){
        printf("Nespravny vstup.\n");
        return 1;
    }
    if (hours >= 24 || hours2 >= 24 || minutes >= 60 || minutes2 >= 60 || secs >= 60 || secs2 >= 60 || msec >= 1000 || msec2 >= 1000){
        printf("Nespravny vstup.\n");
        return 1;
    }
    if (hours < 0 || hours2 < 0 || minutes < 0 || minutes2 < 0 || secs < 0 || secs2 < 0 || msec < 0 || msec2 < 0){
        printf("Nespravny vstup.\n");
        return 1;
    }




    result vysledek;
    vysledek._msec = msec2 - msec;
    if (vysledek._msec < 0){
        vysledek._msec += 1000;
        secs2 --;
    }
    if (secs2 < 0) {
        secs2 += 60;
        minutes2--;
    }

    vysledek._secs = secs2 - secs;
    if (vysledek._secs < 0){
        vysledek._secs += 60;
        minutes2--;
    }
    if (minutes2 < 0 ){
        minutes2 += 60;
        hours2--;
    }

    vysledek._minutes = minutes2 - minutes;
    if (vysledek._minutes < 0){
        vysledek._minutes += 60;
        hours2 --;
    }
    if (hours2 < 0){
        printf("Nespravny vstup.\n");
        return 1;
    }

    vysledek._hours = hours2 - hours;
    if (vysledek._hours < 0){
        printf("Nespravny vstup.\n");
        return 1;
    }



    // dodelat aby to psalo u _msec 000
    printf ("Doba:%3d:%02d:%02d,%03d\n", vysledek._hours , vysledek._minutes , vysledek._secs , vysledek._msec );
    return 0;
}