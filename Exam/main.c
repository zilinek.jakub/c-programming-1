#ifndef __PROGTEST__
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <ctype.h>
#include <limits.h>
#include <assert.h>
#include <stdbool.h>
#endif /* __PROGTEST__ */
#define DEBUG 0
#define COLL_VAL 38
#define COLL_Phase "Core Dump Gold"
typedef struct domElem{
    char * m_Phase;
    int m_Left1;
    int m_Left2;
    int m_Right1;
    int m_Right2;
}domElem;
int qsortDom(const void* a, const void* b){
    domElem arg1 = *(const domElem*)a;
    domElem arg2 = *(const domElem*)b;
    if (strcmp(arg1.m_Phase , arg2.m_Phase) == 0){
        if (arg1.m_Left1 != arg2.m_Left1)
            return arg1.m_Left1 > arg2.m_Left1;
        else if (arg1.m_Left2 != arg2.m_Left2)
            return arg1.m_Left2 > arg2.m_Left2;
        else if (arg1.m_Right1 != arg2.m_Right1)
            return arg1.m_Right1 > arg2.m_Right1;
        else if (arg1.m_Right2 != arg2.m_Right2)
            return arg1.m_Right2 > arg2.m_Right2;
        else
            return strcmp(arg1.m_Phase , arg2.m_Phase);
    }
    return strcmp(arg1.m_Phase , arg2.m_Phase);
}
void Swap(int * val1 , int * val2 , int value1 , int value2 ){
    if (value1 > value2) {
        *val1 = value1;
        *val2 = value2;
    }
    else {
        *val1 = value2;
        *val2 = value1;
    }
}
int getNextBracket(int index, const char *test) {
    while(test[index] != '}'){
        if (test[index] == '\0')
            return ++index;
        index++;
    }
    return ++index;
}
void SwapOneWithTwo(domElem *pElem) {
    if ( pElem->m_Left1 < pElem->m_Right1 ){
        int tmp1 = pElem->m_Left1 , tmp2 = pElem->m_Left2;
        pElem->m_Left1 = pElem->m_Right1;
        pElem->m_Left2 = pElem->m_Right2;
        pElem->m_Right1 = tmp1;
        pElem->m_Right2 = tmp2;
    }
    else if ( pElem->m_Left1 == pElem->m_Right1 &&
              pElem->m_Left2 < pElem->m_Right2 ){
        int tmp1 = pElem->m_Left2;
        pElem->m_Left2 = pElem->m_Right2;
        pElem->m_Right2 = tmp1;
    }
}
void printArr(domElem *arr, int arrIdx) {
    printf("\n\n");
    for (int i = 0; i < arrIdx; i++) {
        printf("%s -> [%d , %d] [%d , %d]\n", arr[i].m_Phase, arr[i].m_Left1, arr[i].m_Left2, arr[i].m_Right1,
               arr[i].m_Right2);
    }
    printf("\n\n");
}
domElem * GetCubes(int * size, const char * input ){
    int index = 0;
    int arrIdx = 0;
    int arrSize = 5;

    domElem * arr = (domElem*)malloc(arrSize*sizeof(domElem));
    while ( true ){
        int LastIndex = index;
        index = getNextBracket(index, input);
        int toAlocate = index - LastIndex;

        if (arrSize - 1 == arrIdx) {
            arr = (domElem *) realloc(arr, sizeof(domElem) * arrSize * 2);
            arrSize *=2;
        }
        arr[arrIdx].m_Phase = (char*)malloc(sizeof(char) * toAlocate);
        int left1, left2 , right1 , right2;
        if (sscanf(&input[LastIndex], " { '%[^\'] ' ; [ %d | %d ] ; [ %d | %d ] }", arr[arrIdx].m_Phase, &left1 , &left2 , &right1 , &right2) != 5) {
            free(arr[arrIdx].m_Phase);
            break;
        }
        Swap(&arr[arrIdx].m_Left1, &arr[arrIdx].m_Left2 , left1 , left2);
        Swap(&arr[arrIdx].m_Right1,&arr[arrIdx].m_Right2, right1 , right2);
        SwapOneWithTwo(&arr[arrIdx]);
        arrIdx ++;
    }
    if (DEBUG)
        printArr(arr , arrIdx);

    *size = arrIdx;
    return arr;
    }
void ArrFree(int arrIdx , domElem * arr ){
    for (int i = 0; i < arrIdx; i++)
        free(arr[i].m_Phase);
    free(arr);
}

bool isCollectible(domElem * elem) {
    if (strcmp(elem->m_Phase , COLL_Phase) == 0){
        if ( ( elem->m_Right2 + elem->m_Right1 == COLL_VAL && elem->m_Left2 + elem->m_Left1 != COLL_VAL ) ||
             ( elem->m_Right2 + elem->m_Right1 != COLL_VAL && elem->m_Left2 + elem->m_Left1 == COLL_VAL ))
            return true;
    }
    return false;
}
int countCollectible (const char * list )
{
    int arrSize;
    domElem * arr = GetCubes( &arrSize , list);
    if (arr == NULL || arrSize == 0)
        return 0;
    int result = 0 ;
    for (int i = 0 ; i < arrSize ; i++){
        if ( isCollectible(&arr[i]) )
            result++;
    }
    ArrFree(arrSize, arr);
    return result;
}

bool CompareInts(int firstLeft, int firstRight, int secondLeft, int secondRight) {
    return firstLeft == secondLeft && secondRight == firstRight;
}

bool isEqual(domElem elem1, domElem elem2) {
//    printf("Compare:\n");
//    printf("%s -> [%d , %d] [%d , %d]\n", elem1.m_Phase, elem1.m_Left1, elem1.m_Left2, elem1.m_Right1,
//           elem1.m_Right2);
//    printf("%s -> [%d , %d] [%d , %d]\n", elem2.m_Phase, elem2.m_Left1, elem2.m_Left2, elem2.m_Right1,
//           elem2.m_Right2);
    bool result = false;
    if (strcmp(elem1.m_Phase , elem2.m_Phase) == 0){
        if(( CompareInts(elem1.m_Left1 , elem1.m_Left2, elem2.m_Left1 , elem2.m_Left2) &&
             CompareInts(elem1.m_Right1 , elem1.m_Right2, elem2.m_Right1 , elem2.m_Right2) ) || (
             CompareInts(elem1.m_Left1 , elem1.m_Left2, elem2.m_Right1 , elem2.m_Right2) &&
             CompareInts(elem1.m_Right1 , elem1.m_Right2, elem2.m_Left1 , elem2.m_Left2) ))
            result = true;
    }
//    printf("vysledek: %d\n\n", result);
    return result;
}

int countUnique  (const char * list){
    int arrSize;
    domElem * arr = GetCubes(&arrSize , list);
    if (arr == NULL || arrSize == 0)
        return 0;
    int result = 0;
    qsort(arr, arrSize, sizeof(domElem), qsortDom);
    if (DEBUG)
        printArr(arr, arrSize);
    for (int i = 1 ; i < arrSize ; i++){
        if ( isEqual(arr[i-1] , arr[i]) ){
            result++;
        }
    }
    result = arrSize - result;
    if (DEBUG)
        printf("result: %d\n", result);
    ArrFree(arrSize , arr);
    return result; /* TODO */
}

uint64_t countTowers ( const char * list )
{
    return 0; /* TODO */
}

#ifndef __PROGTEST__
int main ( void )
{
    const char * str1 =
            " { 'Progtest Exam' ; [ 1 | 2 ] ; [ 3 | 4 ] }"
            " { 'Progtest Exam' ; [ 1 | 2 ] ; [ 3 | 4 ] }"
            "{'PA1 2020/2021';[2|2];[3|1]}\n"
            "{'Progtest Exam' ; [ 2 | 1 ] ; [ 3 | 4 ] }\n"
            "{'Progtest Exam' ; [ 2 | 3 ] ; [ 1 | 4 ] }\n";
        const char * str5 =
            "{'Core Dump Gold' ; [ 38 | 0 ] ; [ 38 | 0 ] }{'PA1 2020/2021';[2|2];[3|1]}\n"
            "{'Core Dump Gold' ; [ 38 | 0 ] ; [ 3  | 4 ] }\n"
            "{'Core Dump Gold' ; [ 3  | 4 ] ; [ 38 | 0 ] }\n"
            "{'Progtest Exam'  ; [ 2 | 3 ] ; [ 1 | 4 ] }\n"
            "{'Core Dump Gold' ; [ 30 | 8 ] ; [ 8 | 29 ] }\n"
            "{'Core Dump Gold' ; [ 8 | 29 ] ; [ 30 | 8 ] }\n"
            "{'Core Dump Gold' ; [ 30 | 8 ] ; [ 30 | 8 ] }\n";
    const char * str2 =
            "{'Crash';  [1|2];[3|4]}\n"
            "{'MemLeak';[1|2];[3|4]}\n"
            "{'MemLeak';[2|3];[3|1]}\n"
            "{'MemLeak';[1|3];[3|2]}\n"
            "{'MemLeak';[1|4];[1|5]}\n"
            "{'MemLeak';[4|1];[1|5]}\n"
            "{'MemLeak';[4|1];[5|1]}\n"
            "{'MemLeak';[1|4];[5|1]}\n"
            "{'MemLeak';[1|5];[1|4]}\n"
            "{'MemLeak';[5|1];[1|4]}\n"
            "{'MemLeak';[5|1];[4|1]}\n"
            "{'MemLeak';[1|5];[4|1]}\n";
    const char * str3 =
            "{'-Wall -pedantic -Werror -Wno-long-long -O2';[2|2];[3|3]}\n"
            "{'-Wall -pedantic -Werror -Wno-long-long -O2';[4|4];[5|5]}\n"
            "{'-Wall -pedantic -Werror -Wno-long-long -O2';[3|4];[4|5]}\n";
    const char * str4 =
            "{'-fsanitize=address -g';[1|5];[5|7]}\n"
            "{'-fsanitize=address -g';[3|7];[1|9]}\n"
            "{'-fsanitize=address -g';[2|2];[4|7]}\n"
            "{'-fsanitize=address -g';[3|9];[2|6]}\n"
            "{'-fsanitize=address -g';[2|2];[2|2]}\n";

//    assert ( countCollectible ( str1 ) == 0 );
//    assert ( countCollectible ( str5) == 4 );
//    countCollectible ( str2);
//    countCollectible ( str3);
//    countCollectible ( str4);
//    assert ( countUnique ( str1 ) == 3 );
    assert ( countUnique ( str2 ) == 4 );
//    assert ( countUnique ( str3 ) == 3 );
//    assert ( countUnique ( str4 ) == 5 );
//    assert ( countTowers ( str3 ) == 10 );
//    assert ( countTowers ( str4 ) == 114 );
    return 0;
}
#endif /* __PROGTEST__ */


/*
Crash -> [4 , 3] [2 , 1]
MemLeak -> [3 , 1] [3 , 2]
MemLeak -> [3 , 2] [3 , 1]
MemLeak -> [4 , 3] [2 , 1]
MemLeak -> [5 , 1] [4 , 1]
MemLeak -> [5 , 1] [4 , 1]
MemLeak -> [5 , 1] [4 , 1]
MemLeak -> [5 , 1] [4 , 1]
MemLeak -> [5 , 1] [4 , 1]
MemLeak -> [5 , 1] [4 , 1]
MemLeak -> [5 , 1] [4 , 1]
MemLeak -> [5 , 1] [4 , 1]

Crash -> [4 , 3] [2 , 1]
MemLeak -> [3 , 1] [3 , 2]
MemLeak -> [3 , 2] [3 , 1]
MemLeak -> [4 , 3] [2 , 1]
MemLeak -> [5 , 1] [4 , 1]
MemLeak -> [5 , 1] [4 , 1]
MemLeak -> [5 , 1] [4 , 1]
MemLeak -> [5 , 1] [4 , 1]
MemLeak -> [5 , 1] [4 , 1]
MemLeak -> [5 , 1] [4 , 1]
MemLeak -> [5 , 1] [4 , 1]
MemLeak -> [5 , 1] [4 , 1]
 */