#include <stdio.h>
#include <math.h>
#include <float.h>

typedef struct Circle{
    double x,y,r;
} Circle;

/** Cases
 *  #1 - kruznice splyvaji TODO: dodelat dopocet prekryvu
 * DONE #2 - jedna kruznice je uvnitr druhe
 *  #3 - Kruznice se zevnitr dotyka te druhe TODO : dodelat ktera je uvnitr a ktera je venkovni
 *  #4 - Protinajici se kruznice TODO: dopocitat jak moc se protinaji
 * DONE #5 - kruznice se dotykaji z venku
 *  #6 - kruznice se vubec nedotykaji
 *  Return values :
 *      669 for calculate and print the full size of circle inside of the other
 *      666 need to calculate 2 points, then connect them and calculate the half and multiply it by 2
 * */


// https://jane111.chytrak.cz/M8/8g_1.3.pdf
int GetPosition (Circle cir1 , Circle cir2 , double v){
    // Vnejsi dotyk
    if( fabs(v - (cir1.r + cir2.r) ) <= (10000 * DBL_EPSILON * (cir1.r + cir2.r) ) ){
        printf("Vnejsi dotyk, zadny prekryv.\n");
    }
        // Kruznice jsou uplne stejne 669
    else if (fabs(cir2.x - cir1.x ) <= (10000 * DBL_EPSILON * 0) &&
             fabs(cir2.y - cir1.y ) <= (10000 * DBL_EPSILON * 0) &&
             fabs(cir2.r - cir1.r ) <= (10000 * DBL_EPSILON * 0)  ){
        printf("Kruznice splyvaji, prekryv: ");
        return 669;
    }
        // Vnitrni dotyky 670
    else if( fabs(v - (cir1.r - cir2.r) ) <= (10000 * DBL_EPSILON *(cir1.r - cir2.r)  ) ) {
        printf("Vnitrni dotyk, kruznice #2 lezi uvnitr kruznice #1, prekryv: ");
        return 670;
    }
    else if( fabs(v - (cir2.r - cir1.r) ) <= (100 * DBL_EPSILON *(cir2.r - cir1.r)  ) ) {
        printf("Vnitrni dotyk, kruznice #1 lezi uvnitr kruznice #2, prekryv: ");
        return 670;
    }

        // Jsou uplne mimo sebe
    else if ( v > cir1.r + cir2.r){
        printf("Kruznice lezi vne sebe, zadny prekryv.\n");
    }

        // jedna lezu uvnitr druhe 670
    else if ( v < (cir1.r - cir2.r) ) { // 3 - 0.8
        printf("Kruznice #2 lezi uvnitr kruznice #1, prekryv: ");
        return 670;
    }
    else if ( v < (cir2.r - cir1.r) ){
        printf("Kruznice #1 lezi uvnitr kruznice #2, prekryv: "); // tohle
        return 670;
    }

        // Protinani 666
    else if ( v < (cir2.r + cir1.r) ){
        ///TODO dodelat prekryv
        printf("Kruznice se protinaji, prekryv: ");
        return 666;
    }
    return 0;
}


int main() {
    Circle cir1;
    Circle cir2;
    printf("Zadejte parametry kruznice #1:\n");
    if ( scanf("%lf %lf %lf", &cir1.x , &cir1.y, &cir1.r ) != 3){
        printf("Nespravny vstup.\n");
        return 1;
    }
    if (cir2.r <= 0 || cir1.r <= 0 ){
        printf("Nespravny vstup.\n");
        return 3;
    }
    printf("Zadejte parametry kruznice #2:\n");
    if ( scanf("%lf %lf %lf", &cir2.x , &cir2.y, &cir2.r ) != 3){
        printf("Nespravny vstup.\n");
        return 2;
    }
    if (cir2.r <= 0 || cir1.r <= 0 ){
        printf("Nespravny vstup.\n");
        return 3;
    }
    //D = distance from centers of circles
    double d = sqrt( ( ( pow(cir2.x - cir1.x , 2) ) + ( ( pow(cir2.y - cir1.y , 2) ) ) ) );
    int result = GetPosition(cir1, cir2, d);
    if ( result == 669 ){
        printf("%f\n" , (M_PI * cir2.r * cir2.r));
    }
    if ( result == 670 ){
        double P2 = M_PI * cir2.r * cir2.r;
        double P1 = M_PI * cir1.r * cir1.r;
        if (P1 < P2)
            printf("%f\n" , P1);
        else
            printf("%f\n" , P2);
    }
    if ( result == 666){

        //Vypocet vysky tetivy
        double m1 = ( ( ( pow(cir1.r,2) - pow(cir2.r,2) ) / (2*d) ) + d/2 );
        double m2 = ( ( ( pow(cir2.r,2) - pow(cir1.r,2) ) / (2*d) ) + d/2 );
        double v1 = ( sqrt( (pow(cir1.r , 2 ) - pow(m1 , 2 ) ) ) );
        double VFull = v1*2;

        //Vypocet uhlu ktere sviraji ramena
        double alfa1 = acos( ( ( (pow(cir1.r,2) * 2) - (pow(VFull, 2) ) )/ (2*cir1.r*cir1.r) ) );
        double alfa2 = acos( ( ( (pow(cir2.r,2) * 2) - (pow(VFull, 2) ) )/ (2*cir2.r*cir2.r) ) );
        double alfa1rad = alfa1;
        double alfa2rad = alfa2;
        alfa1 = alfa1*(180/M_PI);
        alfa2 = alfa2*180/M_PI;
        //Vypocet obsahu    https://cs.wikibooks.org/wiki/Geometrie/Numerick%C3%BD_v%C3%BDpo%C4%8Det_pr%C5%AFniku_dvou_kru%C5%BEnic
        //                  http://vzorce-matematika.hys.cz/obsah-kruhove-usece.php
        double S1 = ( (0.5)*pow(cir1.r,2) )*( ((M_PI*alfa1)/(180)) - (sin(alfa1rad)) ); // 175,
        double S2 = ( (0.5)*pow(cir2.r,2) )*( ((M_PI*alfa2)/(180)) - (sin(alfa2rad)) );
        if (m1 <= 0){
            double S1celek = M_PI * cir1.r*cir1.r;
            S1 = S1celek - S1;
        }
        if (m2 <= 0){
            double S2celek = M_PI * cir2.r *cir2.r;
            S2 = S2celek - S2;
        }
        double S_Total = S1 + S2;
        printf("%lf\n",S_Total);

    }

    return 0;
}