cmake_minimum_required(VERSION 3.17)
project(Progtest5 C)

set(CMAKE_C_STANDARD 11)

add_executable(Progtest5 main.c)