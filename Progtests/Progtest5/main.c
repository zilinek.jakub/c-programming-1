#include <stdio.h>
#include <memory.h>
#include <stdlib.h>

#define debug 0 // used to debug code and print important stuff into console

/** @brief Represents data for each camera record saved in array.*/
typedef struct {
    int m_CamID;
    char m_LP[1001];
    int m_Month;
    char m_MonthString[5];

    int m_Day;
    int m_Hours;
    int m_Min;
    int m_TotalMins;
} camData;

/** @brief Compare function used by qsort, sorts by Licence Plate and then TotalMins.
 *
 *  Used only struct m_Data.
 *  @return int, -1 on lower, 1 on bigger and 0 on same value */
int camDataComparator(const void *a, const void *b) {
    camData arg1 = *(const camData *) a;
    camData arg2 = *(const camData *) b;

    if (strcmp(arg1.m_LP, arg2.m_LP) == 0) {
        if (arg1.m_TotalMins < arg2.m_TotalMins) return -1;
        if (arg1.m_TotalMins > arg2.m_TotalMins) return 1;
        return 0;
    } else
        return strcmp(arg1.m_LP, arg2.m_LP);
}

/** @brief Compare function used by qsort, sorts by int.*/
int cmpInt(const void *a, const void *b) {
    int arg1 = *(const int *) a;
    int arg2 = *(const int *) b;

    if (arg1 < arg2) return -1;
    if (arg1 > arg2) return 1;
    return 0;
}

/** @brief Returns int according to month.
 *
 * @param char * Month = month we want to translate to int index
 * @returns int index of month on success, on fail -1 on invalid month string*/
int getIntFromMonth(char *month) {
    char array[12][4] = {"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};
    for (int i = 0; i < 12; i++)
        if (!strcmp(month, array[i]))
            return i+1;
    return -1;
}

/** @brief Checks date passed in parameter if is valid.
 *
 * @param month = int of month
 * @param day = int of day
 * @param hours = int of hour
 * @param mins = int of minute
 * @returns 0 on invalid date, on success returns 1.
 * */
int validDates(int month, int day, int hours, int mins) {
    if (day <= 0 || mins < 0 || mins >= 60 || hours < 0 || hours >= 24)
        return 0;
    if (month == 2 && day > 28)
        return 0;
    else if ((month == 1 || month == 3 || month == 5 || month == 7 || month == 8 || month == 10 || month == 12) &&
             day > 31)
        return 0;
    else if ((month == 4 || month == 6 || month == 9 || month == 11) && day > 30)
        return 0;
    return 1;
}

/**@brief Calculates total minutes elapsed in time interval month, day, hours and minute
 *
 * @param month = int of month
 * @param day = int of day
 * @param hours = int of hour
 * @param mins = int of minute
 *
 * @returns total minutes elapsed in time interval passed in parameters
 * */
int getMins(int month, int day, int hours, int mins) {
    int result = 0;
    for (int i = 1; i < month; i++) {
        if (i == 2)
            result += 28 * 1440;
        else if (i == 1 || i == 3 || i == 5 || i == 7 || i == 8 || i == 10 || i == 12)
            result += 31 * 1440;
        else
            result += 30 * 1440;
    }
    result += day * 1440;
    result += hours * 60;
    result += mins;
    return result;
}

/** @brief function that prints index of array. This Array must be m_Data type.
 *
 * @param data = array we want to printResultArray from
 * @param idx = index of array we are printing
 * */
void printIndex(camData *data, int idx) {
    printf("[%d] - %d:%s %3s %d %d:%d -> \t%d\n", idx, data[idx].m_CamID, data[idx].m_LP, data[idx].m_MonthString,
           data[idx].m_Day, data[idx].m_Hours, data[idx].m_Min, data[idx].m_TotalMins);
}

/** @brief gets input from user
 *
 * @returns NULL on invalid data, array we created from users data on success
 * @param size = number of loaded indexes on array we returned
 * */
camData *getInput(int *size) {
    camData *data = (camData *) malloc(10 * sizeof(camData));
    int index = 1;
    int baseSize = 10;
    char test1;
    char test2;
    printf("Data z kamer:\n");
    if (scanf(" %c %d : %1000s %3s %d %d : %d %c",
              &test1, &data[0].m_CamID, data[0].m_LP, data[0].m_MonthString,
              &data[0].m_Day, &data[0].m_Hours, &data[0].m_Min, &test2) != 8
        || test1 != '{' || (data[0].m_Month = getIntFromMonth(data[0].m_MonthString)) == -1)
    {
        free(data);
        return NULL;
    }
    else {
        if (validDates(data[0].m_Month, data[0].m_Day, data[0].m_Hours, data[0].m_Min) == 0) {
            free(data);
            return NULL;
        }
        data[0].m_TotalMins = getMins(data[0].m_Month, data[0].m_Day, data[0].m_Hours, data[0].m_Min);
        if (test2 == '}') {
            *size = index;
            return data;
        }
        if (test2 != ',') {
            free(data);
            return NULL;
        }
    }

    int sizeCheck = 0;
    while (1) {
        sizeCheck = scanf(" %d : %1000s %3s %d %d : %d %c",
                          &data[index].m_CamID, data[index].m_LP, data[index].m_MonthString,
                          &data[index].m_Day, &data[index].m_Hours, &data[index].m_Min, &test2);
        if (sizeCheck != 7 || (test2 != '}' && test2 != ',') ||
            (data[index].m_Month = getIntFromMonth(data[index].m_MonthString)) == -1) {
            free(data);
            return NULL;
        }
        if (validDates(data[index].m_Month, data[index].m_Day, data[index].m_Hours, data[index].m_Min) == 0) {
            free(data);
            return NULL;
        }
        data[index].m_TotalMins = getMins(data[index].m_Month, data[index].m_Day, data[index].m_Hours,
                                          data[index].m_Min);

        if (test2 == '}')
            break;

        index++;
        if (index == baseSize) {
            baseSize *= 2;
            data = (camData *) realloc(data, baseSize * sizeof(camData));
        }
    }
    *size = index;
    return data;
}

/**@brief checks if Licence plate is located in array if found changes parameter begin and end ->
 * makes an interval we can find this LP in
 *
 * @param data = array we are searching in, type m_Data
 * @param LP = Licence plate we are searching for
 * @param size = size of data array
 * @param begin = on found changes this to begin of interval
 * @param end = on found changes this to end of interval
 *
 * @return 0 on not found 1 on found-)
 * */
int finderLP(char *LP, camData *data, int size, int *begin, int *end) {
    int Found = 0;
    int FoundInd = -1;
    int LastFoundInd = -1;

    for (int i = 0; i <= size; i++) {
        if (strcmp(LP, data[i].m_LP) == 0) {
            if (!Found) {
                FoundInd = i;
                Found = 1;
            }
        } else if (Found) {
            LastFoundInd = i - 1;
            break;
        }
    }
    *begin = FoundInd;
    if (LastFoundInd == -1)
        LastFoundInd = size;
    *end = LastFoundInd;
    return Found;
}

/** @brief returns int value of closest index for interval begin->end for value of find,
 *  compares m_TotalMinutes and users TotalMinutes, find closest index
 *
 *  @param int begin    = value of the begin index of interval we are searching in
 *  @param int end      = value of the end index of interval we are searching in
 *  @param int find     = value of users minutes we are looking for
 *  @returns int value of closest index for interval begin->end*/
int findClosestIndex(camData *data, int find, int begin, int end) {
    int closest = 0;
    int lowestDiff = 1;
    for (int i = begin; i <= end; i++) {
        if (i == begin) {
            if (data[i].m_TotalMins > find) {
                return i - 1;
            } else {
                lowestDiff = abs(data[i].m_TotalMins - find);
                closest = i;
            }
        } else {
            if (abs(data[i].m_TotalMins - find) <= lowestDiff) {
                lowestDiff = abs(data[i].m_TotalMins - find);
                closest = i;
            }
        }
    }
    if (data[closest].m_TotalMins > find)
        closest--;
    return closest;
}

/**@brief searches in array based on interval - begin and end
 *
 * @param m_Data * data = array we are searching in
 * @param int size = size of array data array
 * @param int begin = begin of interval to search in
 * @param int end = end of interval to search in
 *
 * */

void printResultArray(camData *data, int index, int idx2, const char *string , int * toPrint) {
    printf("%s%s %d %02d:%02d, %dx [", string , data[index + 1].m_MonthString,
           data[index + 1].m_Day, data[index + 1].m_Hours, data[index + 1].m_Min, idx2);
    for (int i = 0; i < idx2; i++) {
        if (i == idx2 - 1)
            printf("%d]\n", toPrint[i]);
        else
            printf("%d, ", toPrint[i]);
    }
}

void getResults(camData *data, int size, int toFind, int begin, int end) {
    int baseSize = 10;
    int baseSize2 = 10;
    int toPrintIdx = 0;
    int toPrintIdx2 = 0;
    int LastFound;
    int *toPrint = (int *) malloc(baseSize * sizeof(int));
    int *toPrint2 = (int *) malloc(baseSize * sizeof(int));
    // searches array for exact time
    for (int i = begin; i <= end; i++) {
        if ((data[i].m_TotalMins - toFind) == 0) {
            toPrint[toPrintIdx] = data[i].m_CamID;
            toPrintIdx++;
            if (toPrintIdx == baseSize) {
                baseSize *= 2;
                toPrint = (int *) realloc(toPrint, baseSize * sizeof(int));
            }
        }
    }
    // if no exact time was found we are looking for closest index
    int closestIndex = findClosestIndex(data, toFind, begin, end);
    if (toPrintIdx == 0) {
        if (debug) {
            printf("Closest : %d\n", closestIndex);
        }
        // if closest index is on begin or before
        if (closestIndex >= 0) {
            LastFound = data[closestIndex].m_TotalMins;
            for (int i = closestIndex; i >= begin; i--) {
                if (i < 0)
                    break;
                if (LastFound != data[i].m_TotalMins)
                    break;
                else {
                    toPrint[toPrintIdx] = data[i].m_CamID;
                    toPrintIdx++;
                    if (toPrintIdx == baseSize) {
                        baseSize *= 2;
                        toPrint = (int *) realloc(toPrint, baseSize * sizeof(int));
                    }
                }
            }
        }
        // if closest index is on the end or after
        if (closestIndex < size) {
            LastFound = data[closestIndex + 1].m_TotalMins;
            for (int i = closestIndex + 1; i <= end; i++) {
                if (i == size + 1)
                    break;
                if (LastFound != data[i].m_TotalMins)
                    break;
                else {
                    toPrint2[toPrintIdx2] = data[i].m_CamID;
                    toPrintIdx2++;
                    if (toPrintIdx2 == baseSize2) {
                        baseSize2 *= 2;
                        toPrint2 = (int *) realloc(toPrint2, baseSize2 * sizeof(int));
                    }
                }
            }
        }
        qsort(toPrint, toPrintIdx, sizeof(int), cmpInt);
        qsort(toPrint2, toPrintIdx2, sizeof(int), cmpInt);


        if (toPrintIdx == 0)
            printf("> Predchazejici: N/A\n");
        else
            printResultArray(data, closestIndex-1, toPrintIdx, "> Predchazejici: " , toPrint);
        if (toPrintIdx2 == 0)
            printf("> Pozdejsi: N/A\n");
        else
            printResultArray(data, closestIndex, toPrintIdx2, "> Pozdejsi: " , toPrint2);
    }
    else {
        qsort(toPrint, toPrintIdx, sizeof(int), cmpInt);
        printResultArray(data, closestIndex-1, toPrintIdx, "> Presne: " , toPrint);
    }
    free(toPrint2);
    free(toPrint);
}

/** @brief function that sorts array, get input from user and finds values in data
 *  User input ends on EOF
 *
 *  @param m_Data * data = array with data we are searching in
 *  @param int size = size of array
 */
void findSolutions(camData *data, int size) {

    qsort(data, size + 1, sizeof(camData), camDataComparator);
    if (debug) {
        for (int i = 0; i <= size; i++)
            printIndex(data, i);
    }
    char *month = (char *) malloc(5);
    char *LP = (char *) malloc(1001);
    int day;
    int hours;
    int minutes;

    printf("Hledani:\n");

    while (1) {
        int result = scanf(" %1000s %3s %d %d : %d", LP, month, &day, &hours, &minutes);
        if (result == EOF)
            break;
        if (result != 5) {
            printf("Nespravny vstup.\n");
            break;
        }
        int monthInt;
        if ((monthInt = getIntFromMonth(month)) == -1) {
            printf("Nespravny vstup.\n");
            break;
        }
        if (!validDates(monthInt, day, hours, minutes)) {
            printf("Nespravny vstup.\n");
            break;
        }
        int userMinutes = getMins(monthInt, day, hours, minutes);
        int begin, end;
        if (finderLP(LP, data, size, &begin, &end) == 0) {
            printf("> Automobil nenalezen.\n");
            continue;
        } else {
            if (debug) {
                printf("Index: %d -> %d\n", begin, end);
            }
            getResults(data, size, userMinutes, begin, end);
        }
    }
    free(month);
    free(LP);
}

int main() {
    int size = 0;
    camData *data;
    if ((data = getInput(&size)) == NULL) {
        printf("Nespravny vstup.\n");
        return -1;
    }
    findSolutions(data, size);
    free(data);
    return 0;
}