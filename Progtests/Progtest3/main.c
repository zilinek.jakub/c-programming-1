#ifndef __PROGTEST__
#include <stdio.h>
#include <assert.h>
#endif /* __PROGTEST__ */

#define cuckoosPerYear 365*180
#define cuckoosPerLeapYear 366*180
#define cuckoosPerDay 180


int dateToIndex(int day, int month, int year, int *idx) {
    if (month > 12 || month <= 0 || day <= 0 || day >= 32)
        return 0;
    int prestupny = 0;

    if (year % 4000 == 0)
        prestupny = 0;
    else if (year % 400 == 0)
        prestupny = 1;
    else if (year % 100 == 0)
        prestupny = 0;
    else if (year % 4 == 0)
        prestupny = 1;


    if ( (prestupny == 0 && month == 2 && day >= 29) ||
         (prestupny == 1 && month == 2 && day >= 30) )
        return 0;
    if ( (month == 3 || month == 1 || month == 5 || month == 7 || month == 8 || month == 10 || month == 12) && day >= 32)
        return 0;
    if ( ( month == 4 || month == 6 || month == 9 || month == 11 ) && day >= 31)
        return 0;


    int month_sum = 0;
    for (int i = 1 ; i < month ; i++){
        if ( i == 2 && prestupny)
            month_sum += 29;
        else if ( i == 2 )
            month_sum += 28;
        else if (i == 3 || i == 1 || i == 5 || i == 7 || i == 8 || i == 10 || i == 12)
            month_sum += 31;
        else
            month_sum += 30;
    }
    int sum = month_sum + day;
    *idx = sum;
    return 1;
}

int cuckoosInTime(int h1, int i1){
    int result = 0;
    if (i1 >= 30){
        result = 1;
    }
    for (int i = h1 ; i >= 0 ; i--) {
        int tmp = i;

        if (i == 0){
            tmp = 24;
            result -= 1;
        }

        if (tmp >= 13){
            tmp -= 12;
        }
        result += 1;
        result += tmp;
    }
    return result;
}

/// Checks for leap year
int IsLeapYear(int year){
    int result = 0;
    if (year % 4000 == 0)
        result = 0;
    else if (year % 400 == 0)
        result = 1;
    else if (year % 100 == 0)
        result = 0;
    else if (year % 4 == 0)
        result = 1;
    return result;
}

/// checks if date is valid on input
int BasicCheck(int y1, int m1, int d1, int h1, int i1,
               int y2, int m2, int d2, int h2, int i2){
    if ( y1 <= 0 ||  m1 <= 0 ||  d1 <= 0 ||  h1 < 0 ||  i1 < 0 ||
         y2 <= 0 ||  m2 <= 0 ||  d2 <= 0 ||  h2 < 0 ||  i2 < 0 )
        return 0;

    if (y1 < 1600 || y2 < 1600)
        return 0;
    // if hours are bigger or even than 24 || minutes are more or even than 60 returns 0;
    if (h1 >= 24 || h2 >= 24 || i1 >= 60 || i2 >= 60 || m1 >= 13 || m2 >= 13)
        return 0;
    // checks if year is LeapYear
    int is_leap_year1 = IsLeapYear(y1);
    int is_leap_year2 = IsLeapYear(y2);
    // validates year and day
    if ( (is_leap_year1 == 0 && m1 == 2 && d1 >= 29) || (is_leap_year2 == 0 && m2 == 2 && d2 >= 29) ||
         (is_leap_year1 == 1 && m1 == 2 && d1 >= 30) || (is_leap_year2 == 1 && m2 == 2 && d2 >= 30))
        return 0;
    if ( ( (m1 == 3 || m1 == 1 || m1 == 5 || m1 == 7 || m1 == 8 || m1 == 10 || m1 == 12) && d1 >= 32 ) ||
         ( (m2 == 3 || m2 == 1 || m2 == 5 || m2 == 7 || m2 == 8 || m2 == 10 || m2 == 12) && d2 >= 32 ) )
        return 0;
    if ( ( ( m1 == 4 || m1 == 6 || m1 == 9 || m1 == 11 ) && d1 >= 31) ||
         ( ( m2 == 4 || m2 == 6 || m2 == 9 || m2 == 11 ) && d2 >= 31) )
        return 0;
    return 1;
}

///returns 1 on valid | returns 0 on invalid
int validateData(int y1, int m1, int d1, int h1, int i1,
                 int y2, int m2, int d2, int h2, int i2 ){
    // year needs to be bigger or even than 1600
    if ( BasicCheck(y1, m1, d1, h1, i1, y2, m2, d2, h2, i2) == 0 )
        return 0;
    if (y1 > y2)
        return 0;
    else if (y1 == y2 && m1 > m2)
        return 0;
    else if (y1 == y2 && m1 == m2 && d1 > d2)
        return 0;
    else if (y1 == y2 && m1 == m2 && d1 == d2 && h1 > h2)
        return 0;
    else if (y1 == y2 && m1 == m2 && d1 == d2 && h1 == h2 && i1 > i2)
        return 0;
    return 1;
}


int cuckooClock ( int y1, int m1, int d1, int h1, int i1,
                  int y2, int m2, int d2, int h2, int i2, long long int * cuckoo ){
    long long result = 0;
    if (  validateData(y1, m1, d1, h1, i1,
                       y2, m2, d2, h2, i2) == 0 )
        return 0;
    if (y1 == y2 && m1 == m2 && d1 == d2 && h1 == h2 && i1 == i2) {
        if (i2 == 30) {
            *cuckoo += 1;
            return 1;
        }
        else if (i2 == 0){
            if (h2 >= 13)
                h2-=12;
            else if (h2 == 0)
                h2 = 12;
            *cuckoo = h2;
            return 1;
        }
        else {
            *cuckoo = 0;
            return 1;
        }
    }
    //Explanation img  https://ctrlv.cz/VCcS
    // od tohoto odecitame
    for (int i = y1 ; i <= y2 ; i++) {
        if (IsLeapYear(i)) {
            result += cuckoosPerLeapYear;
        }
        else {
            result += cuckoosPerYear;
        }
    }

    int dateIndex1 = 0, dateIndex2 = 0;

    int Year2 = 365 + IsLeapYear(y2);

    dateToIndex(d1,m1,y1,&dateIndex1);
    dateToIndex(d2,m2,y2,&dateIndex2);

    Year2 -= dateIndex2;
    result -= (Year2 * cuckoosPerDay);
    result -= (dateIndex1 * cuckoosPerDay);

    long long PlusResult = cuckoosInTime(h2,i2);
    long long NegResult = cuckoosInTime(h1,i1);
    if (i2 == 0) {
        if (h2 >= 13)
            h2 -= 12;
        if (h2 == 0)
            h2 = 12;
        PlusResult += h2;
    }

    result += PlusResult - NegResult;
    *cuckoo = result;
    return 1;
}

#ifndef __PROGTEST__
int main ( int argc, char * argv [] ){
    long long int cuckoo;
    assert ( cuckooClock ( 2000, 12, 31, 0, 0,
                           2000, 12, 31, 0, 0, &cuckoo ) == 1 && cuckoo == 12);
    assert ( cuckooClock ( 2020, 10,  1, 13, 15,
                           2021, 10,  1, 18, 45, &cuckoo ) == 1 && cuckoo == 26+cuckoosPerYear );
    assert ( cuckooClock ( 2020, 10,  1, 13, 15,
                           2020, 10,  2, 11, 20, &cuckoo ) == 1 && cuckoo == 165 );
    assert ( cuckooClock ( 2020,  1,  1, 13, 15,
                           2020, 10,  5, 11, 20, &cuckoo ) == 1 && cuckoo == 50025 );
    assert ( cuckooClock ( 2019,  1,  1, 13, 15,
                           2019, 10,  5, 11, 20, &cuckoo ) == 1 && cuckoo == 49845 );
    assert ( cuckooClock ( 1900,  1,  1, 13, 15,
                           1900, 10,  5, 11, 20, &cuckoo ) == 1 && cuckoo == 49845 );
    assert ( cuckooClock ( 2020, 10,  1,  0,  0,
                           2020, 10,  1, 12,  0, &cuckoo ) == 1 && cuckoo == 102 );
    assert ( cuckooClock ( 2020, 10,  1,  0, 15,
                           2020, 10,  1,  0, 25, &cuckoo ) == 1 && cuckoo == 0 );
    assert ( cuckooClock ( 2020, 10,  1, 12,  0,
                           2020, 10,  1, 12,  0, &cuckoo ) == 1 && cuckoo == 12 );
    assert ( cuckooClock ( 2020, 11,  1, 12,  0,
                           2020, 10,  1, 12,  0, &cuckoo ) == 0 );
    assert ( cuckooClock ( 2020, 10, 32, 12,  0,
                           2020, 11, 10, 12,  0, &cuckoo ) == 0 );
    assert ( cuckooClock ( 2100,  2, 29, 12,  0,
                           2100,  2, 29, 12,  0, &cuckoo ) == 0 );
    assert ( cuckooClock ( 2400,  2, 29, 12,  0,
                           2400,  2, 29, 12,  0, &cuckoo ) == 1 && cuckoo == 12 );
    return 0;
}
#endif /* __PROGTEST__ */