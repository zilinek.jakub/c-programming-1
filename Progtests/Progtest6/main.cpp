#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cctype> // <ctype.h>

#define debug 0

struct node
{
    char * m_Name;
    char * m_Phone;
    node * next;
};

class Counter {
public:
    Counter(){
        m_head = nullptr;
        m_current = nullptr;

        s_number = 10;
        s_name = 10;
        m_name = (char *) malloc(sizeof(char) * s_name);
        m_number = (char *) malloc(sizeof(char) * s_number);

    };

    ~Counter(){
        //TODO free all nodes in list
        free(m_name);
        free(m_number);
    }

    void Start();

private:
    // Linked list nodes
    node * m_head;
    node * m_current;


    char * m_name;
    int s_name;

    char * m_number;
    int s_number;

    char m_operation;

    //Methods
    bool GetInput(bool & end);
    bool Parse(char * string );
    int moveIdx(int current , char * string);
    bool GetNumber(int * idx, char * string);
    bool GetName(int * idx , char * string);
    void WorkManager();
};

void Counter::Start() {
    printf("PBX configuration (+ = set, - = delete, ? = test, EOF = quit):\n");
    bool isAtEnd = false;
     while( true ){
        if ( GetInput(isAtEnd) ){
            if (isAtEnd)
                return;
            WorkManager();
        }
        else
            printf("INVALID COMMAND\n");
    }
}

bool Counter::GetInput(bool & end) {
    char *buffer;
    size_t bufsize = 32;
    buffer = (char *)malloc( bufsize * sizeof(char) );
    size_t size = getline(&buffer, &bufsize, stdin);
    buffer[strlen(buffer) - 1] = '\0';
    // EOF
    if (size == 0) {
        end = true;
        return true;
    }
    if (!Parse(buffer)){
        printf("INVALID COMMAND\n");
    }
    free(buffer);
    /* TODO Parse the actual input */
    return true;
}

bool Counter::Parse(char *string) {
    if (string[0] != '+' && string[0] != '-' && string[0] != '?'  || strlen(string) < 8 || string[1] != ' ')
        return false;
    m_operation = string[0];
    if (m_operation == '+') {
        int idx = moveIdx(1, string);
        if (!GetNumber(&idx, string))
            return false;
        idx = moveIdx(idx, string);
        if (string[idx] != '\"')
            return false;
        idx++;
        GetName(&idx, string);
        if (string[idx] != '\"' || string[idx+1] != '\n')
            return false;
        printf("Result -> %s %s\n",m_number,m_name);
    }
    else if (m_operation == '-'){
        int idx = moveIdx(1, string);
        if (!GetNumber(&idx, string))
            return false;
        printf("Result -> %s\n",m_number);
    }
    else if (m_operation == '?'){
        int idx = moveIdx(1, string);
        if (!GetNumber(&idx, string))
            return false;
        printf("Result -> %s\n",m_number);
    }
    return true;
}

int Counter::moveIdx(int current, char * string) {
    while(string[current] == ' '){
        current ++;
    }
    return current;
}

bool Counter::GetNumber(int * idx, char * string){
    int writeIdx = 0;

    for (int i = *idx ;  ; i++ ){
        if (string[i] == ' ' || i == strlen(string)) {
            *idx = i;
            m_number[writeIdx] = '\0';
            return true;
        }
        if ( string[i] < '0' || string[i] > '9' )
            return false;
        else{
            if (writeIdx == s_number - 1){
                s_number *= 2;
                m_number = (char*)realloc(m_number , s_number * sizeof(char) );
            }
            m_number[writeIdx] = string[i];
            writeIdx++;
        }

    }
}

bool Counter::GetName(int *idx, char *string) {
    int writeIdx = 0;
    bool isSlash = false;
    for (int i = *idx ; ; i++){
        if (string[i] == '\0')
            return false;
        if (isSlash){
            if (s_name == writeIdx - 1) {
                s_name *= 2;
                m_name = (char *) realloc(m_name, s_name * sizeof(char));
            }
            m_name[writeIdx] = string[i];
            writeIdx++;
            isSlash = false;
        }
        else {
            if (string[i] == '\"') {
                m_name[writeIdx] = '\0';
                *idx = i;
                break;
            }
            if (string[i] == '/') {
                isSlash = true;
            } else
                isSlash = false;
            if (s_name == writeIdx - 1) {
                s_name *= 2;
                m_name = (char *) realloc(m_name, s_name * sizeof(char));
            }
            if (!isSlash) {
                m_name[writeIdx] = string[i];
                writeIdx++;
            }
        }
    }
    return true;
}

void Counter::WorkManager() {

}


int main() {
    Counter app{};
    app.Start();
    return 0;
}