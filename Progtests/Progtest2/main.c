#include <stdio.h>
#include <math.h>


/// Returns 0 if invalid, if valid we return 1, if EOF return 2
int getInput(int * _start , int * _end){
    int EOF_test = scanf("%d %d", _start , _end);
    if (EOF_test == EOF)
        return 2;
    else if ( EOF_test != 2 || *_start > *_end || *_start <= 0 ) {
        return 0;
    }
    return 1;
}

int int_pow(int first, int second){
    int tmp = first;
    for (int i = 1 ; i != second ; i++)
        tmp *= first;
    return tmp;
}

int main() {
    printf("Intervaly:\n");
    while(1) {
        int _start, _end;
        int Input = getInput(&_start, &_end);
        if (Input == 0) {
            printf("Nespravny vstup.\n");
            return 1;
        }
        else if (Input == 2) {
            return 0;
        }


        int count = 0;
        if (_start == 1)
            count++;
        for (int i = _start; i <= _end; i++) { // jde od startu do konce

            for (int j = 2; j <= (int) sqrt(i); j++) { // prochazi zaklady j = 2 sqrt(i)
                int found = 0;
                for (int k = 2, tmp;; k++) { // exponent
                    tmp = (int_pow((int) j, (int) k));
                    if (tmp > i)
                        break;
                    if (tmp == i) {
                        //printf("found: %d^%d\n",j , k);
                        found = 1;
                        break;
                    }
                }
                if (found) {
                    count++;
                    break;
                }
            }
        }
        //printf("\nPocet perfektnich mocnic: %d\n" , count );
        printf("<%d;%d> -> %d\n", _start, _end, ((_end - _start - count + 1)));
    }
}