#include <stdio.h>
#include <stdlib.h>

int CompFun(const void* a, const void* b){
    return (*(const int*)a > *(const int*)b) - (*(const int*)a < *(const int*)b);
}
/** @return Loads data from used and stores them into Array, if not fail returns size of data that was loaded */
int GetInput(int * Array){
    int counter = 0;
    printf("Delky nosniku:\n");
    for (; counter < 10000 ; counter++){
        int result = scanf("%d", &Array[counter]);
        if (result == EOF)
            break;
        else if ( result != 1 || counter > 10000 || Array[counter] <= 0 )
            return counter; // TODO RETURN -1
    }
    if (counter <= 2)
        return -1;
    else
        return counter;
}

int CountTriangles(int * Array, int size) {
    qsort(Array, size, sizeof(int), CompFun);
    int count = 0;
    int VarCheck = -1;
    for (int i = 0 ; i < size ; i++){ // todo maybe -2\-1
        if (VarCheck == Array[i])
            continue;
        VarCheck = Array[i];
        int VarCheck2 = -1;
        for (int j = i + 1; j < size ; j ++){ // todo maybe -1
            if (VarCheck2 == Array[j])
                continue;
            VarCheck2 = Array[j];
            int VarCheck3 = -1;
            for (int k = j + 1 ; k < size; k++) { // // todo maybe -1/-2
                if (VarCheck3 == Array[k])
                    continue;
                VarCheck3 = Array[k];
                if (VarCheck + VarCheck2 > VarCheck3) {
//                    printf("%d %d %d\n", VarCheck,VarCheck2,VarCheck3);
                    count++;
                }
            }
        }
    }
    return count;
}

int main() {
    int buffer[10000];
    int result = GetInput(buffer);
    if (result == -1 ) {
        printf("Nespravny vstup.\n");
        return 1;
    }
    printf("Trojuhelniku: %d\n", CountTriangles(buffer, result));
    return 0;
}