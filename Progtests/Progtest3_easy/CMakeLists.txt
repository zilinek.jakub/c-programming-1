cmake_minimum_required(VERSION 3.17)
project(Progtest3_easy C)

set(CMAKE_C_STANDARD 11)

add_executable(Progtest3_easy main.c)