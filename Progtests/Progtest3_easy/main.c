#ifndef __PROGTEST__
#include <stdio.h>
#include <limits.h>
#include <assert.h>
#include <stdint.h>
#endif /* __PROGTEST__ */

int mulDiv ( uint64_t a, uint64_t b, uint64_t c, uint64_t *q, uint64_t * r )
{
    if (c == 0)
        return 0;

    *q = ( (a*b)/c );
    return 1;
}

#ifndef __PROGTEST__
int main (int argc, char * argv [])
{
    uint64_t q, r;
    assert ( mulDiv ( 100, 200, 250, &q, &r ) == 1
             && q == 80
             && r == 0 );
    assert ( mulDiv ( 100, 200, 300, &q, &r ) == 1
             && q == 66
             && r == 200 );
    assert ( mulDiv ( 500, 10, 0, &q, &r ) == 0 );
    assert ( mulDiv ( UINT64_C(1) << 35, UINT64_C(1) << 42, 16384, &q, &r ) == 1
             && q == UINT64_C(1) << 63
             && r == 0 );
    assert ( mulDiv ( UINT64_C(1) << 35, UINT64_C(1) << 42, 8192, &q, &r ) == 0 );
    assert ( mulDiv ( UINT64_MAX, UINT64_MAX, UINT64_MAX, &q, &r ) == 1
             && q == UINT64_MAX
             && r == 0 );
    return 0;
}
#endif /* __PROGTEST__ */

